class PostsController < ApplicationController

  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])

    @images = @post.image != '' ? JSON.parse(@post.image) : nil
  end

  def new
    @post = Post.new
  end

  def edit
    @id = params[:id];
    @post = Post.find(params[:id])

    @images = @post.image != '' ? JSON.parse(@post.image) : nil
  end

  def create
    @post = Post.new(posts_params)
    if @post.save
      redirect_to posts_path
    else
      render :new
    end
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(posts_params)
      redirect_to posts_path
    else
      render :edit
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to posts_path
  end

  private

  def posts_params
    params.require(:post).permit(:link, :title, :image)
  end
end
